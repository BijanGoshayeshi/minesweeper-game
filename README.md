# Minesweeper Game

In this project, a GUI Python game is developed to be similar to the Minesweeper game.


## Specific considerations

1- Single player game.

2- Computer will randomly choose squares to place the bombs.

3- If the player loses the game by pressing on a bomb, then a new game starts with the same grid and mine field.


## How to run

Please in your prefered Python IDE open the "Main" folder and run the "main.py".
