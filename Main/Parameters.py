# minesweeper properties
# Number of columns and rows
# Max number of buttons at each direction is 10
MS_N_Col = 10
MS_N_Row = 10
if (MS_N_Col > 10):
    MS_N_Col = 10
if (MS_N_Row > 10):
    MS_N_Row = 10
NumberofCell = MS_N_Col * MS_N_Row
RemainedCells = NumberofCell
# Number of Mines
MS_N_Mines = MS_N_Col*MS_N_Row // 6
