from Parameters import *

Canvas_width = 800
Canvas_length = 1200
Main_Section_Width = Canvas_width*0.98
Main_Section_Length = Canvas_length * 0.75
Main_Canvas_start_width = Canvas_width*0.01

Button_size_Lenght = 8
Button_size_Width = 4

# A list to hold all the generated boxes
Box_list = []
