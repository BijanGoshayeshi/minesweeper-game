from tkinter import Frame, Label, messagebox
from Canvas import *
from Classes import *
import random
import os.path
import time


def hello():
    messagebox.showinfo("Welcome to the Minesweeper Game",
                        "Please use mouse right click to detect a mine \n and left mouse click to detect an empty box.")


def Start_Screen(canvas, length, width):
    canvas.configure(bg="black")
    screen_resolution = str(length)+'x'+str(width)
    canvas.geometry(screen_resolution)
    canvas.title("MineSweeper Game")
    canvas.resizable(False, False)


def Canvas_Main_Section(canvas):
    main_section = Frame(canvas, bg="#D7E5F0",
                         height=Main_Section_Width, width=Main_Section_Length)
    main_section.place(x=0, y=Main_Canvas_start_width)
    return main_section


def Canvas_Margin_Section(canvas):
    margin_section = Frame(canvas, bg="#ADD8E6",
                           height=Main_Section_Width, width=Canvas_length-Main_Section_Length)
    margin_section.place(x=Main_Section_Length, y=Main_Canvas_start_width)
    return margin_section


def GameBoxBoard_Generation(canvas, Section=None, GamBoxCol=1, GameBoxRow=1, if_reset=False):
    for i in range(GamBoxCol):
        for j in range(GameBoxRow):
            Box_temp = Box(canvas, Section, i, j)
            Box_temp.GenerateBox()
    MineDistributer(Box_list, if_reset)
    hello()


def MineDistributer(someBoxes, same_seed=False):
    if (same_seed == False):
        seed_val = random.randrange(1000000000)
        f = open("./temp_files/seed.txt", "w")
        f.write(str(seed_val))
        f.close()
    else:
        print("read state seed ", same_seed)
        f = open("./temp_files/seed.txt", "r")
        seedstr = f.readline()
        f.close()
        seed_val = int(seedstr)
        print("seed is read from file", seed_val)
        # time.sleep(4)

    random.seed(seed_val)
    MineBoxes = random.sample(Box_list, MS_N_Mines)
    for Box in MineBoxes:
        Box.Exploded = True


def CheckResetart_incase_Explosion():
    file_exists = os.path.exists('./temp_files/GameState.txt')
    if file_exists:

        infile = open('./temp_files/GameState.txt', 'r')
        firstLine = infile.readline()
        infile.close()
        f = open("./temp_files/GameState.txt", "w")
        f.write("")
        f.close()
        if firstLine == "True":
            return True
        else:
            return False

    return False
