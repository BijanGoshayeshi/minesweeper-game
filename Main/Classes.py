from argparse import Action
from doctest import master
from ssl import ALERT_DESCRIPTION_NO_RENEGOTIATION
from tkinter import *
from tkinter import messagebox
from Canvas import *
from Parameters import *
from Functions import *
import os
import sys
import time


class Box:
    Box_rem_count = None

    def __init__(self, canvas, Section=None, ColX=0, ColY=0):
        self.section = Section
        self.canvas = canvas
        self.X = ColX
        self.Y = ColY
        self.Exploded = False  # Explosion brings Game Over
        # Mouse Left Click make a box opened if the guess is correct and no explosion occurs
        self.Opened = False
        self.Mine_Detected = False  # Right Mouse click to guess if a box is a mine
        self.button = None

    def GenerateBox(self):
        self.button = Button(self.section)
        self.button.grid(column=self.X, row=self.Y)
        self.button.config(height=Button_size_Width, width=Button_size_Lenght)
        self.button.configure(bg="#D7E5F0")
        Box_list.append(self)
        self.button.bind('<Button-1>', self.ActionOnLeftClick)
        self.button.bind('<Button-3>', self.ActionOnRightClick)

    @staticmethod
    def WhichBox(X, Y):
        for box in Box_list:
            if X == box.X and Y == box.Y:
                return box

    def NeighborBoxes(self):
        NeighBoxes = []
        thisX = self.X
        thisY = self.Y
        Minecounter = 0
        for i in (-1, 0, 1):
            for j in (-1, 0, 1):
                if (i == 0 and j == 0):
                    pass
                else:
                    newX = thisX + i
                    newY = thisY + j
                    foundBox = self.WhichBox(newX, newY)
                    if foundBox != None:
                        NeighBoxes.append(foundBox)
                        if (foundBox.Exploded):
                            Minecounter += 1
        return NeighBoxes, Minecounter

    def RevealMineNumbers(self):
        NeighBoxes, MineCounts = self.NeighborBoxes()
        self.button.configure(text=MineCounts)
        return NeighBoxes, MineCounts

    @staticmethod
    def MarginLabeler(Section, X, Y, message):
        label = Label(Section, text=message, font=("", 23))
        label.place(x=X, y=Y)
        Box.Box_rem_count = label

    def OpenBox(self):
        self.button.configure(bg="#D7E5F0")
        global RemainedCells
        if self.Opened == False:
            self.button.unbind('<Button-1>')
            self.button.unbind('<Button-3>')
            RemainedCells -= 1
            self.Opened = True
            if Box.Box_rem_count:
                Box.Box_rem_count.configure(
                    text="Remained open cells:\n {} - {}  ".format(RemainedCells, MS_N_Mines))

    def ActionOnLeftClick(self, ActionArg):
        if self.Exploded:
            # We have encountered a mine explosion and losing the game!
            self.button.configure(bg="red")
            messagebox.showinfo("Game Over: Mine Explosion Occurs",
                                "The Game will restart again.\nHowever the mine field will be the same.")
            self.canvas.destroy()
            f = open("./temp_files/GameState.txt", "w")
            f.write("True")
            f.close()
            # time.sleep(2)
            python = sys.executable
            os.execl(python, python, * sys.argv)

        else:
            self.OpenBox()

            NeighBoxes, MineCounts = self.RevealMineNumbers()
            if MineCounts == 0:
                for Boxes in NeighBoxes:
                    Boxes.OpenBox()
                    a, b = Boxes.RevealMineNumbers()
            if RemainedCells == MS_N_Mines:
                messagebox.showinfo("Congratulations!",
                                    "You Won The Game!")

    def ActionOnRightClick(self, ActionArg):
        if self.Mine_Detected == False:
            self.button.configure(bg="green")
            self.Mine_Detected = True
        else:
            self.button.configure(bg="#D7E5F0")
            self.Mine_Detected = False
