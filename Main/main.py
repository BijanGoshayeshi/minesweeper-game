from pdb import Restart
from symbol import parameters
from tkinter import *
import tkinter.messagebox
from Canvas import *
from Functions import *
from Classes import *

ResetChecker = CheckResetart_incase_Explosion()

master = Tk()

Start_Screen(master, Canvas_length, Canvas_width)
main_section = Canvas_Main_Section(master)
margin_section = Canvas_Margin_Section(master)
GameBoxBoard_Generation(master, main_section, MS_N_Col, MS_N_Row, ResetChecker)
Box.MarginLabeler(margin_section, 0, 0,
                  "Remained open cells:\n {} - {}  ".format(RemainedCells, MS_N_Mines))


master.mainloop()

# //Final line
